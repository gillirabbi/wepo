var cntx;
var p;
var p2;
var drawing = false;
var typing = false;
var pencilarr = [];

$(document).ready(function() {
    cntx = $('#canvas')[0].getContext('2d');
    textBox.destroy();
    textTool.hide();
});

$('.field').mouseup(function() {
    if ($('#tool').val() === "text")
        textTool.spawn();
    else
        textTool.hide();
});

$('#canvas').mousedown(function(e) {
    p = getMouseCoords(e);
    drawing = true;
    textBox.destroy();
    switch($('#tool').val()) {
        case "pencil":
            pencilarr.push(p);
            break;
        case "text":
            drawing = false;
            typing = true;
            textBox.spawn(e);
            break;
    }
});

$('#canvas').mousemove(function(e) {
    if (drawing === true) {
        draw();
        switch($('#tool').val()) {
            case "pencil":
                pencilarr.push(getMouseCoords(e));
                drawShape.pencil(pencilarr, {
                            color: getProperty.color(0.5),
                            size: getProperty.size()
                    });
                break;
            case "circle":
                drawShape.circle(p, getMouseCoords(e), {
                        color: getProperty.color(0.5),
                        size: getProperty.size()
                    });
                break;
            case "rect":
                drawShape.rect(p, getMouseCoords(e), {
                        color: getProperty.color(0.5),
                        size: getProperty.size()
                    });
                break;
            case "line":
                drawShape.line(p, getMouseCoords(e), {
                        color: getProperty.color(0.5),
                        size: getProperty.size()
                    });
                break;
        }
    }
});

$('#canvas').mouseup(function(e) {
    p2 = getMouseCoords(e);
    switch($('#tool').val()) {
        case "pencil":
            undo_queue.push(
                new DrawPencil(pencilarr,
                    new Properties(getProperty.color(), getProperty.size()
                )
            ));
            break;
        case "circle":
            undo_queue.push(
                new DrawObj(p, p2, drawShape.circle,
                    new Properties(getProperty.color(), getProperty.size()
                )
            ));
            break;
        case "rect":
            undo_queue.push(
                new DrawObj(p, p2, drawShape.rect,
                    new Properties(getProperty.color(), getProperty.size()
                )
            ));
            break;
        case "line":
            undo_queue.push(
                new DrawObj(p, p2, drawShape.line,
                    new Properties(getProperty.color(), getProperty.size()
                )
            ));
            break;
    }
    drawing = false;
    pencilarr = [];
    draw();
});

$(document).keydown(function(e) {
    if (typing === true) {
        switch(e.keyCode) {
            case 13: // key: enter
                var str = $('.text').val();
                textBox.destroy();
                typing = false;
                undo_queue.push(new DrawText(p, str,
                    new Properties(getProperty.color(),
                                   getProperty.size(),
                                   0,
                                   getProperty.font())
                ));
                draw();
                break;
            case 27: // key: esc
                textBox.destroy();
                typing = false;
                break;
        }
    }else{
        switch(e.keyCode) {
            case 49: // key: 1
                $('#tool').val('pencil');
                textTool.hide();
                break;
            case 50: // key: 2
                $('#tool').val('circle');
                textTool.hide();
                break;
            case 51: // key: 3
                $('#tool').val('rect');
                textTool.hide();
                break;
            case 52: // key: 4
                $('#tool').val('line');
                textTool.hide();
                break;
            case 53: // key: 5
                $('#tool').val('text');
                textTool.spawn();
                break;
            case 90: // if 'z' is pressed, undo
                if (undo_queue.length > 0)
                    redo_queue.push(undo_queue.pop());
                draw();
                break;
            case 88: // if 'x' is pressed. redo
                if (redo_queue.length > 0)
                    undo_queue.push(redo_queue.pop());
                draw();
                break;
        }
    }
    console.log(e.keyCode);
});

$('#btnSave').click(function(e) {
    var stringifiedArray = JSON.stringify(undo_queue);
    var param = { 
            "user": "gisli13",
            "name": "drawing",
            "content": stringifiedArray,
            "template": true
    };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "http://whiteboard.apphb.com/Home/Save",
        data: param,
        dataType: "jsonp",
        crossDomain: true,
        success: function (data) {
            console.log(data);
        },
        error: function (xhr, err) {
            alert("Unable to save drawing, please try again.");
        }
    });
});
