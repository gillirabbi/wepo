var undo_queue = [];
var redo_queue = [];
var WIDTH = $('#canvas').width();
var HEIGHT = $('#canvas').height();

function draw() {
    cntx.clearRect(0, 0, WIDTH, HEIGHT);
    for (var i = 0; i < undo_queue.length; i++) {
        undo_queue[i].put();
    }
    console.log(undo_queue);
}

function Point(x, y) {
    this.x = x;
    this.y = y;
}

function Properties(color, size, alpha, font) {
    this.color = color;
    this.size = size;
    this.alpha = alpha;
    this.font = font;
}


var DrawPencil = function(pointarr, properties) {
    this.pointarr = pointarr;
    this.properties = properties;
    this.put = function() {
        drawShape.pencil(this.pointarr, this.properties);
    };
};

var DrawObj = function(PointA, PointB, fun, properties) {
    this.a = PointA;
    this.b = PointB;
    this.properties = properties;
    this.put = function() {
        fun(this.a, this.b, this.properties);
    };
};

var DrawText = function(Point, str, properties) {
    this.p = Point;
    this.str = str;
    this.properties = properties;
    this.put = function() {
        drawShape.text(this.p, this.str, this.properties);
    };
};

function getMouseCoords(e) {
    var x = e.offsetX;
    var y = e.offsetY;
    return new Point(x, y);
}

var textBox = {
    spawn: function(e) {
        var x = event.pageX;
        var y = event.pageY;

        var text = $('.text');
        text.show();
        text.css("position", "fixed");
        text.css("top", y);
        text.css("left", x);
        // focus workaround
        setTimeout(function() {
            text.focus();
        }, 0);
    },

    destroy: function() {
        var text = $('.text');
        text.val("");
        text.hide();
    }
};

var textTool = {
    spawn: function() {
        $('#textTool').show();
        $('.fontsize').attr({ id: "size" });
        $('.fontsize').show();
        $('.strokesize').attr({ id: "size2" });
        $('.strokesize').hide();
    },

    hide: function() {
        $('#textTool').hide();
        $('.strokesize').attr({ id: "size" });
        $('.strokesize').show();
        $('.fontsize').attr({ id: "size2" });
        $('.fontsize').hide();
    }
};

var getProperty = {
    color: function(a) {
        this.a = ( a === undefined ) ? 1 : a;
        switch ($('#color').val()) {
            case "black":
                return "rgba(0, 0, 0, " + this.a + ")";
            case "red":
                return "rgba(255, 0, 0, " + this.a + ")";
            case "green":
                return "rgba(0, 255, 0, " + this.a + ")";
            case "blue":
                return "rgba(0, 0, 255, " + this.a + ")";
        }
    },

    size: function() {
        return $('#size').val();
    },

    font: function() {
        return $('#font').val();
    }
};

var drawShape = {
    pencil: function(pointarr, properties) {
        cntx.beginPath();
        cntx.moveTo(pointarr[0].x, pointarr[0].y);
        for (var i = 1; i < pointarr.length; i++) {
            cntx.lineTo(pointarr[i].x, pointarr[i].y);
        }
        cntx.lineWidth = properties.size;
        cntx.strokeStyle = properties.color;
        cntx.stroke();
        cntx.closePath();
        cntx.lineWidth = 1;
        cntx.strokeStyle = "#000";
    },

    line: function(PointA, PointB, properties) {
        cntx.beginPath();
        cntx.moveTo(PointA.x, PointA.y);
        cntx.lineTo(PointB.x, PointB.y);
        cntx.lineWidth = properties.size;
        cntx.strokeStyle = properties.color;
        cntx.stroke();
        cntx.closePath();
        cntx.lineWidth = 1;
        cntx.strokeStyle = "#000";
    },

    // PointB should be the end point, not the size
    rect: function(PointA, PointB, properties) {
        cntx.beginPath();
        cntx.rect(PointA.x, PointA.y, (PointB.x - PointA.x), (PointB.y - PointA.y));
        cntx.lineWidth = properties.size;
        cntx.strokeStyle = properties.color;
        cntx.stroke();
        cntx.closePath();
        cntx.lineWidth = 1;
        cntx.strokeStyle = "#000";
    },

    // The radius of the circle will be the distance from PointA to PointB
    circle: function(PointA, PointB, properties) {
        var dist = Math.sqrt(
            Math.pow((PointB.x - PointA.x), 2) +
            Math.pow((PointB.y - PointA.y), 2));
        cntx.beginPath();
        cntx.arc(PointA.x, PointA.y, dist, 0, 2 * Math.PI);
        cntx.lineWidth = properties.size;
        cntx.strokeStyle = properties.color;
        cntx.stroke();
        cntx.closePath();
        cntx.lineWidth = 1;
        cntx.strokeStyle = "#000";
    },

    text: function(Point, str, properties) {
        cntx.beginPath();
        cntx.font = properties.size + "px " + properties.font;
        cntx.fillStyle = properties.color;
        cntx.fillText(str, Point.x, Point.y);
        cntx.closePath();
        cntx.fillStyle = "#000";
    }
};
