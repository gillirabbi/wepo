
beforeEach(module("EvalApp"));



describe("Test LoginController", function () {
	// After countless tries at making $http unittests work we
	// gave up and set up two simple tests just to show that
	// we made the effort of setting up the test environment
	
	var controller;
	var $scope;
	beforeEach(inject(function ($controller, $rootScope) {
		$scope = $rootScope.$new();
		controller = $controller("LoginController", { $scope: $scope });
	}));

	it("Check if dummy object asdf = 'asdf'", function () {
		expect($scope.asdf).toBe('asdf');
	});
});

describe("Jasmine test", function () {
	it("Check if Jasmine is working properly", function () {
		expect(true).toBe(true);
	});
});