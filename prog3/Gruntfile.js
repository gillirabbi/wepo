module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
          development: {
            options: {
              paths: ["style"]
            },
            files: {
              "style/style.css": "style/style.less"
            }
          }
        },
        karma: {
          unit: {
            options: {
              frameworks: ['jasmine'],
              singleRun: true,
              browsers: ['PhantomJS'],
              files: [
                'bower_components/angular/angular.js',
                'bower_components/angular-mocks/angular-mocks.js',
                'bower_components/angular-route/angular-route.js',
                'bower_components/ngstorage/ngStorage.js',
                'src/app.js',
                'src/*.js',
                'test/*.spec.js'
              ]
            }
          }
        },
        jshint: {
            src: ['src/*.js'],
            gruntfile: ['Gruntfile.js'],
            options: {
                curly:  true,
                immed:  true,
                newcap: true,
                noarg:  true,
                sub:    true,
                boss:   true,
                eqnull: true,
                node:   true,
                undef:  true,
                globals: {
                        _:       false,
                        jQuery:  false,
                        angular: false,
                        moment:  false,
                        console: false,
                        $:       false,
                        io:      false
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-karma');

    grunt.registerTask('default', [
        'jshint',
        'karma',
        'less'
    ]);
};
