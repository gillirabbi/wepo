angular.module('EvalApp').controller('NewEvaluationController', function ($scope, $http, $location, SERVER_URL, UserService) {
	if (!UserService.checkadmin()) {
		$location.path('/my');
	}
	$http({method: 'GET', url: SERVER_URL + '/api/v1/evaluationtemplates', headers: { 'Authorization': 'Basic ' + UserService.getToken() } })
	.success(function (data) {
		$scope.evaluationtemplates = data;
	});

	$scope.submitNewEval = function() {
		$http({
			method: 'POST', 
			url: SERVER_URL + '/api/v1/evaluations', 
			headers: { 'Authorization' : 'Basic ' + UserService.getToken() },
			data: {
				TemplateID: $scope.TemplateID,
				StartDate: $scope.StartDate.toISOString(),
				EndDate: $scope.EndDate.toISOString()
			}
		})
		.success(function() {
			$location.path('/evaluations');
		});
	};
});