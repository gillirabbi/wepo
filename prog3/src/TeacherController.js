angular.module('EvalApp').controller('TeacherController', function ($scope, $http, $rootScope, $routeParams, SERVER_URL, UserService) {
	$scope.course = $routeParams.course;
	$http({
		method: 'GET', 
		url: SERVER_URL + '/api/v1/courses/' + $routeParams.course + '/' + $routeParams.semester + '/teachers', 
		headers: { 
			'Authorization': 'Basic ' + UserService.getToken() 
		} 
	})
	.success(function (data) {
		$scope.teachers = data;
	});
});