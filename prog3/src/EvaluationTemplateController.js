angular.module('EvalApp').controller('EvaluationTemplateController', function ($scope, $http, $routeParams, $location, SERVER_URL, UserService) {
	if (!UserService.checkadmin()) {
		$location.path('/my');
	}
	$http({method: 'GET', url: SERVER_URL + '/api/v1/evaluationtemplates/' + $routeParams.id, headers: { 'Authorization': 'Basic ' + UserService.getToken() } })
	.success(function (data) {
		$scope.evaluationtemplate = data;
	});
});