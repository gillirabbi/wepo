angular.module('EvalApp').controller('MyController', function ($scope, $http, $rootScope, SERVER_URL, UserService) {
	$scope.username = $rootScope.username;
	$scope.admin = UserService.checkadmin();

	$http({method: 'GET', url: SERVER_URL + '/api/v1/my/courses', headers: { 'Authorization': 'Basic ' + UserService.getToken() } })
	.success(function (data) {
		$scope.courses = data;
	});
	$http({method: 'GET', url: SERVER_URL + '/api/v1/my/evaluations', headers: { 'Authorization': 'Basic ' + UserService.getToken() } })
	.success(function (data) {
		$scope.evaluations = data;
	});
});