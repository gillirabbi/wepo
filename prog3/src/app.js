angular.module('EvalApp', ['ngRoute', 'ngStorage']);

angular.module('EvalApp')
.config(
	function ($routeProvider) {
		$routeProvider
			.when('/login', { templateUrl: 'views/login.html', controller: 'LoginController' })
			.when('/my', { templateUrl: 'views/my.html', controller: 'MyController' })
			.when('/evaluationtemplates', { templateUrl: 'views/evaluationtemplates.html', controller: 'EvaluationTemplatesController' })
			.when('/evaluationtemplates/new', { templateUrl: 'views/newevaluationtemplate.html', controller: 'NewEvaluationTemplateController' })
			.when('/evaluationtemplates/:id', { templateUrl: 'views/evaluationtemplate.html', controller: 'EvaluationTemplateController' })
			.when('/evaluations', { templateUrl: 'views/evaluations.html', controller: 'EvaluationsController' })
			.when('/evaluations/new', { templateUrl: 'views/newevaluation.html', controller: 'NewEvaluationController' })
			.when('/evaluations/:id', { templateUrl: 'views/evaluation.html', controller: 'EvaluationController' })
			.when('/courses/:course/:semester/teachers', { templateUrl: 'views/teachers.html', controller: 'TeacherController' })
			.when('/courses/:course/:semester/evaluations/:id', { templateUrl: 'views/submitevaluation.html', controller: 'SubmitEvaluationController' })
			.otherwise({ redirectTo: '/login' });
	}
)
.constant("SERVER_URL", "http://dispatch.ru.is/h39");

angular.module('EvalApp').service('UserService', function ($sessionStorage) {
	this.setToken = function(token) {
		$sessionStorage.userToken = token;
	};
	this.getToken = function() {
		return $sessionStorage.userToken;
	};
	this.setAdmin = function(user) {
		if (user.Role === "admin") {
			$sessionStorage.admin = true;
		}else{
			$sessionStorage.admin = false;
		}
	};
	this.checkadmin = function() {
		return $sessionStorage.admin;
	};
});

angular.module('EvalApp').directive('ngEnter', function () {
	return function (scope, element, attrs) {
		element.bind("keydown keypress", function (event) {
			if(event.which === 13) {
				scope.$apply(function (){
					scope.$eval(attrs.ngEnter);
			});
			event.preventDefault();
			}
		});
	};
});

angular.module('EvalApp').directive('autofocus', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link : function($scope, $element) {
			$timeout(function() {
				$element[0].focus();
			});
		}
	};
}]);

angular.module('EvalApp').directive('evaluationQuestion', function () { // Custom directive for an evaluation question
	return {
		template: '{{question.Text}} - {{question.TextEN}}'
	};
});