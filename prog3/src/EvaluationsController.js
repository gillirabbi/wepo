angular.module('EvalApp').controller('EvaluationsController', function ($scope, $http, $location, SERVER_URL, UserService) {
	if (!UserService.checkadmin()) {
		$location.path('/my');
	}
	$http({method: 'GET', url: SERVER_URL + '/api/v1/evaluations', headers: { 'Authorization': 'Basic ' + UserService.getToken() } })
	.success(function (data) {
		$scope.evaluations = data;
	});
});