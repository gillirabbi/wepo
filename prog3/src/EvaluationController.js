angular.module('EvalApp').controller('EvaluationController', function ($scope, $http, $location, $routeParams, SERVER_URL, UserService) {
	if (!UserService.checkadmin()) {
		$location.path('/my');
	}
	$http({method: 'GET', url: SERVER_URL + '/api/v1/evaluations/' + $routeParams.id, headers: { 'Authorization': 'Basic ' + UserService.getToken() } })
	.success(function (data) {
		$scope.evaluation = data;
	});
});