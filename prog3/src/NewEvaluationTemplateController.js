angular.module('EvalApp').controller('NewEvaluationTemplateController', function ($scope, $http, $location, SERVER_URL, UserService) {
	if (!UserService.checkadmin()) {
		$location.path('/my');
	}
	$scope.cquestions = [];
	$scope.tquestions = [];
	$scope.addQuestion = function (type) {
		var arr;
		if (type === 'course') {
			$scope.cquestions.push({
				Text: '',
				TextEN: '',
				Type: 'Text',
				Answers: []
			});
		}else{
			$scope.tquestions.push({
			Text: '',
			TextEN: '',
			Type: 'Text',
			Answers: []
		});
		}
		
	};
	$scope.addAnswer = function (question) {
		question.Answers.push({
			Text: '',
			TextEN: '',
			Weight: 1
		});
	};

	$scope.submitNewEvalTemplate = function () {
		var req = {
			method: 'POST',
			url: SERVER_URL + '/api/v1/evaluationtemplates',
			headers: {
				'Authorization': 'Basic ' + UserService.getToken()
			},
			data: {
				Title: $scope.Title,
				TitleEN: $scope.TitleEN,
				IntroText: $scope.IntroText,
				IntroTextEN: $scope.IntroTextEN,
				CourseQuestions: $scope.cquestions,
				TeacherQuestions: $scope.tquestions
			}
		};
		$http(req)
		.success(function (data) {
			$location.path('/evaluationtemplates');
		});
	};
});