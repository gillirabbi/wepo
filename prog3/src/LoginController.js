angular.module('EvalApp').controller('LoginController', function ($scope, $http, $location, $rootScope, SERVER_URL, UserService) {
	$scope.login = function(user, pass) {
		$http.post(SERVER_URL + '/api/v1/login', { user: user, pass: pass })
		.success(function(data) {
			UserService.setToken(data.Token);
			UserService.setAdmin(data.User);
			$rootScope.username = data.User.FullName;
			$location.path('/my');
		});
	};
	$scope.asdf = 'asdf';
});