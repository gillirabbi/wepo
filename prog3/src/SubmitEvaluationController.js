angular.module('EvalApp').controller('SubmitEvaluationController', function ($scope, $http, $rootScope, $routeParams, $location, SERVER_URL, UserService) {
	$scope.cquestions = [];
	$scope.tquestions = [];
	$scope.submitquestions = [];
	$scope.course = $routeParams.course;
	$http({
		method: 'GET', 
		url: SERVER_URL + '/api/v1/courses/' + $routeParams.course + '/' + $routeParams.semester + '/evaluations/' + $routeParams.id, 
		headers: { 
			'Authorization': 'Basic ' + UserService.getToken() 
		} 
	})
	.success(function (data) {
		$scope.evaluation = data;
	});

	$http({
		method: 'GET', 
		url: SERVER_URL + '/api/v1/courses/' + $routeParams.course + '/' + $routeParams.semester + '/teachers', 
		headers: { 
			'Authorization': 'Basic ' + UserService.getToken() 
		} 
	})
	.success(function (data) {
		$scope.teachers = data;
	});

	$scope.submit = function () {
		angular.forEach($scope.cquestions, function (obj, key) {
			$scope.submitquestions.push(obj);
		});
		angular.forEach($scope.tquestions, function (arr, key) {
			angular.forEach(arr, function (obj, key) {
				$scope.submitquestions.push(obj);
			});
		});
		$http({
			method: 'POST', 
			url: SERVER_URL + '/api/v1/courses/' + $routeParams.course + '/' + $routeParams.semester + '/evaluations/' + $routeParams.id, 
			headers: { 'Authorization': 'Basic ' + UserService.getToken() },
			data: $scope.submitquestions
		})
		.success(function() {
			$location.path('/my');
		});
	};
});