window.Bottom = (function() {
	'use strict';

	var SPEED = 15;

	var Bottom = function(el, game) {
		this.el = el;
		this.game = game;
		this.pos = { x: 0, y: 0 };
	};

	Bottom.prototype.onFrame = function(delta) {
		if (this.pos.x < 0) {
			this.pos.x = this.game.WORLD_WIDTH;
		}
		this.pos.x -= delta * SPEED;
		this.el.css('background-position', this.pos.x + 'em ' + this.pos.y + 'em');
	};

	return Bottom;
})();