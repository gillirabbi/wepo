window.Player = (function() {
	'use strict';

	var Controls = window.Controls;

	// All these constants are in em's, multiply by 10 pixels
	// for 1024x576px canvas.
	var SPEED = 30; // * 10 pixels per second
	// var WIDTH = 5;
	var HEIGHT = 7;
	var INITIAL_POSITION_X = 30;
	var INITIAL_POSITION_Y = 25;
	var rot = 0;

	var Player = function(el, game) {
		this.el = el;
		this.game = game;
		this.pos = { x: 0, y: 0 };
	};

	/**
	 * Resets the state of the player for a new game.
	 */
	Player.prototype.reset = function() {
		this.pos.x = INITIAL_POSITION_X;
		this.pos.y = INITIAL_POSITION_Y;
	};

	Player.prototype.onFrame = function(delta) {
		if (Controls.keys.up || Controls.keys.space) {
			if (!this.game.mute) {
				var snd = new Audio('../assets/jump.ogg');
				snd.play();
			}
			this.pos.y -= delta * (SPEED * 4);
			if(rot > -25) {
				rot -= 10;
			}
		}else{
			if(rot < 35) {
				rot += 2;
			}
		}

		this.pos.y += delta * (SPEED / 1.5);

		this.checkCollisionWithBounds();

		// Update UI
		this.el.css('transform', 'translate3d(' + INITIAL_POSITION_X + 'em, ' + this.pos.y + 'em, 0) rotate(' + rot + 'deg)');
	};

	Player.prototype.checkCollisionWithBounds = function() {
		if (this.pos.y < 0 ||
			this.pos.y + HEIGHT > this.game.WORLD_HEIGHT - this.game.BOT_HEIGHT) {
			return this.game.gameover();
		}
	};

	return Player;

})();
