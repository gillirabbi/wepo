window.Background = (function() {
	'use strict';

	var Background = function(el, game, speed) {
		this.el = el;
		this.game = game;
		this.speed = speed;
		this.pos = { x: 0, y: 0 };
	};

	Background.prototype.onFrame = function(delta) {
		this.pos.x -= delta * this.speed;
		this.el.css('background-position', this.pos.x + 'em ' + this.pos.y + 'em');
	};

	return Background;
})();