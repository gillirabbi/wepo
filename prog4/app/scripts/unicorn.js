window.Unicorn = (function() {
	'use strict';

	var Unicorn = function(el, game, speed) {
		this.el = el;
		this.game = game;
		this.speed = speed;
		this.pos = { x: this.game.WORLD_WIDTH + 15, y: 0 };
	};

	Unicorn.prototype.onFrame = function(delta) {
		if (this.pos.x + 40 < 0) {
			this.pos.x = this.game.WORLD_WIDTH + 15;
		}
		this.pos.x -= delta * this.speed;
		this.el.css('transform', 'translate3d(' + this.pos.x + 'em, ' + this.pos.y + 'em, 0)');
	};

	return Unicorn;
})();