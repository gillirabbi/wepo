window.Pipe = (function() {
	'use strict';

	var PLAYER_WIDTH = 7;
	var SPEED = 15;
	var DELAY = 15; // Delay until first pipe
	var RAN_MAX = 10;
	var RAN_MIN = -10;

	var Pipe = function(el, player, game, xOffset) {
		this.rand = Math.floor(Math.random() * (RAN_MAX - RAN_MIN + 1)) + RAN_MIN;
		this.el = el;
		this.player = player;
		this.game = game;
		this.initialx = xOffset + this.game.WORLD_WIDTH + DELAY;
		this.pos = { x: this.initialx, y: this.rand };
		this.called = false; // Has the addPoint been called?
	};

	Pipe.prototype.onFrame = function(delta) {
		if (this.pos.x < -15) {
			this.pos.x = this.game.WORLD_WIDTH;
			this.rand = Math.floor(Math.random() * (RAN_MAX - RAN_MIN + 1)) + RAN_MIN;
			this.pos.y = this.rand;
			this.called = false;
		}

		this.checkCollision();
		this.pos.x -= delta * SPEED;
		this.el.css('transform', 'translate(' + this.pos.x + 'em, ' + this.pos.y + 'em)');
	};

	Pipe.prototype.reset = function() {
		this.pos.x = this.initialx;
		this.called = false;
	};

	Pipe.prototype.checkCollision = function() {
		if (this.player.pos.x + PLAYER_WIDTH > this.pos.x && this.player.pos.x - PLAYER_WIDTH < this.pos.x) {
			if (this.player.pos.y < this.rand + 20 || this.player.pos.y + PLAYER_WIDTH > this.rand + 37.6) {
				return this.game.gameover();
			}
		}else if (this.player.pos.x > this.pos.x + PLAYER_WIDTH && this.player.pos.x < this.pos.x + 25) {
			if (!this.called) { // Can only be called once unless reset
				this.game.addPoint();
				if (!this.game.mute) {
					var snd = new Audio('../assets/coin.ogg');
					snd.play();
				}
				this.called = true;
			}
		}
	};

	return Pipe;
})();