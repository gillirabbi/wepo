
window.Game = (function() {
	'use strict';

	var dogeMessage = ['Wow! such play!', 'Very flappy!', 'Good Doge!', 'Many fun!', 'Such meme!', 'How kitten!', 'Much lame!'];

	/**
	 * Main game class.
	 * @param {Element} el jQuery element containing the game.
	 * @constructor
	 */
	var Game = function(el) {
		this.el = el;
		this.player = new window.Player(this.el.find('.Player'), this);
		this.bottom = new window.Bottom(this.el.find('.Bottom'), this); // Bottom
		this.background = new window.Background(this.el.find('.Background'), this, 9);
		this.unicorn = new window.Unicorn(this.el.find('.Unicorn'), this, 12);
		this.pipe1 = new window.Pipe(this.el.find('.Pipe1'), this.player, this, 29.35);
		this.pipe2 = new window.Pipe(this.el.find('.Pipe2'), this.player, this, 58.7);
		this.pipe3 = new window.Pipe(this.el.find('.Pipe3'), this.player, this, 88.05);
		this.pipe4 = new window.Pipe(this.el.find('.Pipe4'), this.player, this, 117.4);
		this.isPlaying = false;
		this.mute = false;

		this.points = 0;
		document.getElementById('audio-player').volume = 0.25; // Set music volume to quarter by default

		// Cache a bound onFrame since we need it each frame.
		this.onFrame = this.onFrame.bind(this);
	};

	/**
	 * Runs every frame. Calculates a delta and allows each game
	 * entity to update itself.
	 */
	Game.prototype.onFrame = function() {
		// Check if the game loop should stop.
		if (!this.isPlaying) {
			return;
		}

		// Calculate how long since last frame in seconds.
		var now = +new Date() / 1000,
				delta = now - this.lastFrame;
		this.lastFrame = now;

		// Update game entities.
		this.player.onFrame(delta);
		this.bottom.onFrame(delta); // Bottom
		this.background.onFrame(delta);
		this.unicorn.onFrame(delta);
		this.pipe1.onFrame(delta);
		this.pipe2.onFrame(delta);
		this.pipe3.onFrame(delta);
		this.pipe4.onFrame(delta);

		// Request next frame.
		window.requestAnimationFrame(this.onFrame);
	};

	Game.prototype.addPoint = function() {
		this.points++;
	};

	/**
	 * Starts a new game.
	 */
	Game.prototype.start = function() {
		this.reset();

		// Restart the onFrame loop
		this.lastFrame = +new Date() / 1000;
		window.requestAnimationFrame(this.onFrame);
		this.isPlaying = true;
	};

	/**
	 * Resets the state of the game so a new game can be started.
	 */
	Game.prototype.reset = function() {
		this.player.reset();
		this.pipe1.reset();
		this.pipe2.reset();
		this.pipe3.reset();
		this.pipe4.reset();
		this.points = 0;
	};

	/**
	 * Signals that the game is over.
	 */
	Game.prototype.gameover = function() {
		this.isPlaying = false;

		var that = this;
		var muteBoard = this.el.find('.Mute');
		var muteSound = this.el.find('.Mute-sound');
		var muteMusic = this.el.find('.Mute-music');
		var scoreboardEl = this.el.find('.Scoreboard');
		var scoreboardTxt = this.el.find('.Scoreboard-text');
		var audioPlayer = document.getElementById('audio-player');
		muteBoard.slideDown();
		scoreboardTxt
			.html(dogeMessage[Math.floor(Math.random()*dogeMessage.length)] + '<br/>' + this.points + ' points!');
		muteSound
			.off('click')
			.on('click', function() {
				console.log('mutes');
				that.mute = that.mute ? false : true;
				muteSound.toggleClass('Mute-muted');
			});
		muteMusic
			.off('click')
			.on('click', function() {
				console.log('mutem');
				audioPlayer.muted = audioPlayer.muted ? false: true;
				muteMusic.toggleClass('Mute-muted');
			});
		scoreboardEl
			.addClass('is-visible')
			.find('.Scoreboard-restart')
				.one('click', function() {
					scoreboardEl.removeClass('is-visible');
					muteBoard.slideUp();
					that.start();
				});
	};

	/**
	 * Some shared constants.
	 */
	Game.prototype.WORLD_WIDTH = 102.4;
	Game.prototype.WORLD_HEIGHT = 57.6;
	Game.prototype.BOT_HEIGHT = 7; // Height of bottom from world edge

	return Game;
})();


