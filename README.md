# README #

# T-427-WEPO - Vefforitun II #
## Reykjavík University - Spring 2015 ##

### Description ###

Repository containing WEPO projects - client-side web programming with emphasis on:

- HTML5
- JavaScript
- Single-Page Application (SPA) using AngularJS
- CSS
- Responsive web design