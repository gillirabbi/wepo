module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            build: ['js/ngmin', 'js/concat', 'js/min/main.min.js'],
            dependencies: ['bower_components', 'node_modules']
        },
        run: {
            server: {
                cmd: './start-server.bat'
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['js/ngmin/app.js', 'js/ngmin/*.js'],
                dest: 'js/concat/main.js'
            }
        },
        ngmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'js',
                    src: '*.js',
                    dest: 'js/ngmin'
                }]
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'js/min/main.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },
        jshint: {
            src: ['js/*.js'],
            gruntfile: ['Gruntfile.js'],
            options: {
                curly:  true,
                immed:  true,
                newcap: true,
                noarg:  true,
                sub:    true,
                boss:   true,
                eqnull: true,
                node:   true,
                undef:  true,
                globals: {
                        _:       false,
                        jQuery:  false,
                        angular: false,
                        moment:  false,
                        console: false,
                        $:       false,
                        io:      false
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-ngmin');
    grunt.loadNpmTasks('grunt-run');

    grunt.registerTask('test', ['jshint']);
    grunt.registerTask('clean.dependencies', ['clean:dependencies']);
    grunt.registerTask('default', [
        'jshint', 
        'clean:build',
        'ngmin', 
        'concat', 
        'uglify',
        'run:server'
    ]);
};
