angular.module('ChatClient').controller('RoomsController', function ($scope, $location, $rootScope, $routeParams, $window, socket) {
	$scope.errorMessage = '';
	$scope.currentUser = $routeParams.user;
	
	socket.on('roomlist', function (data) {
			$scope.rooms = Object.keys(data);
	});
	
	socket.on('userlist', function (users) {
		$scope.currentUsers = users;
	});
	
	socket.emit('rooms');
	socket.emit('users');

	socket.on('recv_privatemsg', function (user, message) {
		$scope.privateUser = user;
		$scope.privateMsg = message;
		$scope.displayprivatereply = true;
		$scope.displayprivate = false;
	});

	$scope.showprivate = function (user) {
		$scope.displayprivate = true;
		$scope.displayprivatereply = false;
		$scope.privateuser = user;
	};

	$scope.sendprivate = function (user) {
		if ($scope.newprivatemsg !== undefined && $scope.newprivatemsg !== null) {
			socket.emit('privatemsg', { nick: user, message: $scope.newprivatemsg }, function (success) {
				if (!success) {
					$scope.errorMessage = "Could not send private message";
				}
			});
		}
		$scope.newprivatemsg = undefined;
		$scope.displayprivate = false;
	};

	$scope.replyprivate = function (user) {
		if ($scope.newprivatereplymsg !== undefined && $scope.newprivatereplymsg !== null) {
			socket.emit('privatemsg', { nick: user, message: $scope.newprivatereplymsg }, function (success) {
				if (!success) {
					$scope.errorMessage = "Could not send private message";
				}
			});
		}
		$scope.newprivatereplymsg = undefined;
		$scope.displayprivatereply = false;
	};

	$scope.createnewroom = function () {
		if ($scope.newroom === undefined || $scope.newroom === null) {
			$scope.errorMessage = 'Please specify a name for the lobby';
		} else if (checklobbies($scope.newroom, $scope.rooms)) {
			$scope.errorMessage = "This name is already taken!";
		} else {
			$window.location.href = "#/room/" + $scope.currentUser + "/" + $scope.newroom;
		}
	};

	function checklobbies (newroom, rooms) {
		var check = false;
		for (var i = 0; i < rooms.length; i++) {
			if (rooms[i] === newroom) {
				check = true;
				break;
			}
		}
		return check;
	}
});