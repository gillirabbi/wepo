angular.module('ChatClient').controller('RoomController', function ($scope, $location, $rootScope, $routeParams, $window, socket) {
	$scope.currentRoom = $routeParams.room;
	$scope.currentUser = $routeParams.user;
	$scope.currentUsers = [];
	$scope.currentOps = [];
	$scope.serverMessages = [];
	$scope.errorMessage = '';
	$scope.privateMsg = '';
	$scope.displayprivate = false;
	$scope.displayprivatereply = false;

	socket.on('kicked', function (room, user) {
		if (user === $scope.currentUser) {
			$window.location.href = "#/rooms/" + $scope.currentUser;
		}
	});

	socket.on('banned', function (room, user) {
		if (user === $scope.currentUser) {
			$window.location.href = "#/rooms/" + $scope.currentUser;
		}
	});

	socket.on('updateusers', function (roomName, users, ops) {
		if (roomName === $scope.currentRoom) {
			$scope.currentUsers = users;
		}
		if (roomName === $scope.currentRoom) {
			$scope.currentOps = ops;
		}
	});	

	socket.on('updatechat', function (roomName, data) {
		if (roomName === $scope.currentRoom) {
			$scope.chat = data;
		}
	});

	socket.on('recv_privatemsg', function (user, message) {
		$scope.privateUser = user;
		$scope.privateMsg = message;
		$scope.displayprivatereply = true;
		$scope.displayprivate = false;
	});

	socket.on('servermessage', function (type, room, user) {
		if (room === $scope.currentRoom) {
			if (type === 'join') {
				$scope.serverMessages.push(user + " has joined " + room);
			}
			if (type === 'part') {
				$scope.serverMessages.push(user + " has left " + room);
			}
			if (type === 'quit') {
				$scope.serverMessages.push(user + " has disconnected");
			}

		}
	});

	socket.emit('joinroom', { room: $scope.currentRoom }, function (success, reason) {
		if (!success) {
			$scope.errorMessage = reason;
		}
	});

	$scope.showprivate = function (user) {
		$scope.displayprivate = true;
		$scope.displayprivatereply = false;
		$scope.privateuser = user;
	};

	$scope.sendprivate = function (user) {
		if ($scope.newprivatemsg !== undefined && $scope.newprivatemsg !== null) {
			socket.emit('privatemsg', { nick: user, message: $scope.newprivatemsg }, function (success) {
				if (!success) {
					$scope.errorMessage = "Could not send private message";
				}
			});
		}
		$scope.newprivatemsg = undefined;
		$scope.displayprivate = false;
	};

	$scope.replyprivate = function (user) {
		if ($scope.newprivatereplymsg !== undefined && $scope.newprivatereplymsg !== null) {
			socket.emit('privatemsg', { nick: user, message: $scope.newprivatereplymsg }, function (success) {
				if (!success) {
					$scope.errorMessage = "Could not send private message";
				}
			});
		}
		$scope.newprivatereplymsg = undefined;
		$scope.displayprivatereply = false;
	};

	$scope.kickuser = function (user) {
		socket.emit('kick', { user: user, room: $scope.currentRoom }, function (success) {
			if (!success) {
				$scope.errorMessage = "User could not be kicked";
			}
		});
	};

	$scope.banuser = function (user) {
		socket.emit('ban', { user: user, room: $scope.currentRoom }, function (success) {
			if (!success) {
				$scope.errorMessage = "User could not be banned";
			}
		});
	};

	$scope.opuser = function (user) {
		socket.emit('op', { user: user, room: $scope.currentRoom }, function (success) {
			if (!success) {
				$scope.errorMessage = "User could not be opped";
			}
		});
	};

	$scope.deopuser = function (user) {
		socket.emit('deop', { user: user, room: $scope.currentRoom }, function (success) {
			if (!success) {
				$scope.errorMessage = "User could not be deoped";
			}
		});
	};

	$scope.checkifop = function () {
		return $scope.currentOps.hasOwnProperty($scope.currentUser);
	};

	$scope.leaveroom = function () {
		socket.emit('partroom', $scope.currentRoom);
	};
	
	$scope.sendmessage = function () {
		if ($scope.newmessage !== undefined && $scope.newmessage !== null) {
			socket.emit('sendmsg', { roomName: $scope.currentRoom, msg: $scope.newmessage });
			$scope.newmessage = undefined;
		}
	};
});