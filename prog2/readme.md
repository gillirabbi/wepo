Chat Application
====

## Installation
To install the app `node`, `npm`, `grunt-cli` and `bower` must be installed. Provided that these application are present, the app can be installed by running
```
npm install -d
bower install
```

The server is now ready to be launched.

### Starting  the app
The next thing to do is to start the http client, which is done by running:
```
./start-client.bat&
```
Adding the `&` at the end starts it in the background.

By calling 
```
grunt
```
the grunt task runner will lint everything with *jshint*, clean the relevant build locations, and start minifying using *ngmin*. Then it will concatenate and uglify everything into a file called `main.min.js`. Finally grunt will start up the node server.

Grunt will **not** include the chatserver.js file in the build to avoid complications.

Ngmin is used to avoid *uglify* losing Angular specific information. Alternatively starting the server can be done with
```
./start-server.bat
```
however, grunt must be run once first to build the project.

### Uninstalling dependencies
All dependencies can be removed by using the command
```
grunt clean.dependencies
```

The whole project can be uninstalled simply by removing the whole directory.

## How to use
The application is very simple, a user simply logs in with his or her deciered username. The user can now chose a chatroom to join or create their own. If the user creates their own chatroom they automatically have op (operator) powers, enabling them to kick, ban or give other users op. An op can also deop other users.

Private messaging is rather simple in the app. To message another user, simply click on their name and write the message in the field that appears. A user that receives a private message can then reply in the field that pops up on their screen. 

**Note** that only one private message is displayed to a user at a time, the user will not be able to see a private chat log. The system is instead intended to be used more like text messaging where the user can send another user a short private message which the other user can chose to reply to or ignore.

## Known issues
The server will crash if the page is refreshed. The app should therefore be navigated without using the refresh button.